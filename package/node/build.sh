#!/bin/bash
#trap "exit" SIGHUP SIGINT SIGTERM
#-------------------------------------------------------------------------
# Install dependencies
#-------------------------------------------------------------------------
#sudo apt-get install -y build-essential
#sudo apt-get install -y subversion
#sudo apt-get install -y git-core
#sudo apt-get install -y patch
#sudo apt-get install -y bzip2
#sudo apt-get install -y flex
#sudo apt-get install -y bison
#sudo apt-get install -y autoconf
#sudo apt-get install -y gettext
#sudo apt-get install -y unzip
#sudo apt-get install -y libncurses5-dev
#sudo apt-get install -y ncurses-term
#sudo apt-get install -y zlib1g-dev
#sudo apt-get install -y gawk
#sudo apt-get install -y libz-dev
#sudo apt-get install -y libssl-dev
#-------------------------------------------------------------------------
# Prepare OpenWrt SDK
#-------------------------------------------------------------------------
# OpenWrt Attitude Adjustment : 12.09
# https://dev.openwrt.org/log/branches/attitude_adjustment?mode=follow_copy
#svn checkout -r 41803 svn://svn.openwrt.org/openwrt/branches/attitude_adjustment openwrtsource
#cd openwrtsource
#./scripts/feeds update -a
#./scripts/feeds install -a
#make defconfig
#make prereq
#make menuconfig
#cp ../.config ./
#time make V=99
#cd ..
#echo "Please enter some input: "
#read input_variable
#-------------------------------------------------------------------------
# Cross-compile V8 and Node.js
#-------------------------------------------------------------------------
# Get custom version of v8m-rb with patch to MIPS platfform
#git clone https://github.com/paul99/v8m-rb.git -b dm-dev-mipsbe
# Get node.js version 0.10.5
#git clone https://github.com/joyent/node.git -b v0.10.5-release node-v0.10-5-mips
# Save current path
export BASEDIR=$(pwd)
 
export STAGING_DIR=/home/suse13.2-32bit/barrier_breakersvn44295/staging_dir
export PREFIX=${STAGING_DIR}/toolchain-mips_34kc_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mips-openwrt-linux-
export CC=${PREFIX}gcc
export CXX=${PREFIX}g++
export AR=${PREFIX}ar
export RANLIB=${PREFIX}ranlib
export LINK=${PREFIX}g++
export CPP="${PREFIX}gcc -E"
export STRIP=${PREFIX}strip
export OBJCOPY=${PREFIX}objcopy
export LD=${PREFIX}g++
export OBJDUMP=${PREFIX}objdump
export NM=${PREFIX}nm
export AS=${PREFIX}as
export PS1="[${PREFIX}] \w$ "
 
export LIBPATH=${STAGING_DIR}/toolchain-mips_34kc_gcc-4.8-linaro_uClibc-0.9.33.2/lib/
export LDFLAGS='-Wl,-rpath-link '${LIBPATH}
export GYPFLAGS="-Dv8_use_mips_abi_hardfloat=false -Dv8_can_use_fpu_instructions=false"
make clean
make dependencies
# build the version for compiling node
make mips.release werror=no library=shared snapshot=off -j4

# lib will be compiled here => v8m-rb/out/mips.release/lib.target/libv8.so
#echo "Please enter some input: "
#read input_variable
# build node.js with support to v8 mips shared library
cd ${NODEJSSOURCE}
./configure --without-snapshot --shared-v8 --shared-v8-includes=${V8SOURCE}/include/ --shared-v8-libpath=${V8SOURCE}/out/mips.release/lib.target --dest-cpu=mips
time make snapshot=off -j1
# copy node.js and libv8.so MIPS and deploy
# ${NODEJSSOURCE}/out/Release/node
# /home/seven/work/openwrt/node-v0.10-5-mips/out
# ${V8SOURCE}/out/mips.release/lib.target/libv8.so
cd node-v0.10.28/
export BASEDIR=/home/suse13.2-32bit/barrier_breakersvn44295
export STAGING_DIR=/home/suse13.2-32bit/barrier_breakersvn44295/staging_dir
 
export V8SOURCE=${BASEDIR}/package/node/v8m-rb
 
export PREFIX=${STAGING_DIR}/toolchain-mips_34kc_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mips-openwrt-linux-
 
export LIBPATH=${STAGING_DIR}/toolchain-mips_34kc_gcc-4.8-linaro_uClibc-0.9.33.2/lib/
 
# MIPS cross-compile exports
export CC=${PREFIX}gcc
export CXX=${PREFIX}g++
export AR=${PREFIX}ar
export RANLIB=${PREFIX}ranlib
export LINK=${PREFIX}g++
export CPP="${PREFIX}gcc -E"
export STRIP=${PREFIX}strip
export OBJCOPY=${PREFIX}objcopy
export LD=${PREFIX}g++
export OBJDUMP=${PREFIX}objdump
export NM=${PREFIX}nm
export AS=${PREFIX}as
export LDFLAGS='-Wl,-rpath-link '${LIBPATH}
export TARGET_PATH=${BASEDIR}/nodeone/opt
export GYPFLAGS="-Dv8_use_mips_abi_hardfloat=false -Dv8_can_use_fpu_instructions=false"
make clean
make distclean
./configure --dest-cpu=mips --dest-os=linux  --with-mips-float-abi=soft --without-snapshot --shared-v8 --shared-v8-includes=${V8SOURCE}/include/ --shared-v8-libpath=${V8SOURCE}/out/mips.release/lib.target/
make snapshot=off -j4
make install
#-------------------------------------------------------------------------
# Deploy cross-compiled V8 and Node.js
#-------------------------------------------------------------------------
cd ${NODEJS_MIPS_AR9331}
mkdir nodejs_deploy
cp node-v0.10-5-mips/out/Release/node nodejs_deploy/
cp v8m-rb/out/mips.release/lib.target/libv8.so nodejs_deploy/
file nodejs_deploy/node
file nodejs_deploy/libv8.so